package Database;

import java.io.*;

public class CreatingFiles {

    public static File NikeAirMax;
    public static File SK8_HI;
    public static File programmerTShirt;
    public static File secondTShirt;
    public static File watch;
    public static File sunglasses;
    public static CreatingFiles cf;
    private BufferedWriter bw;

    private void assigningReferances(File file){
        try {
            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
        }catch (Exception e){
            System.out.println("It is not working");
        }
    }

    public void creatingNikeAirMax(String ... a){
        NikeAirMax = new File("Nike Air Max.txt");
        assigningReferances(NikeAirMax);
        try {
            //name
            bw.write("Nike Air Max \n");
            //color
            bw.write("Black\n");
            //--
            bw.write(" - \n");
            //price
            bw.write("49.90 AZN \n");
            //adding
            bw.write(a[0]+"\n");
            bw.close();
        }catch (Exception e){
            System.out.println("Data has not been written!");
        }
    }

    public void creatingSK8_HI(String ... a){
        SK8_HI = new File("SK8_HI.txt");
        assigningReferances(SK8_HI);
        try {
            //name
            bw.write("SK8_HI \n");
            //color
            bw.write("Red\n");
            //--
            bw.write(" - \n");
            //price
            bw.write("69.90 AZN \n");
            //adding
            bw.write(a[0]+"\n");
            bw.close();
        }catch (Exception e){
            System.out.println("Data has not been written!");
        }
    }

    public void creatingProgrammerTShirt(String ... a){
        programmerTShirt = new File("Programmer T-Shirt.txt");
        assigningReferances(programmerTShirt);
        try {
            //name
            bw.write("Programmer T-Shirt \n");
            //color
            bw.write("Black\n");
            //--
            bw.write(" - \n");
            //price
            bw.write("24.90 AZN \n");
            //adding
            bw.write(a[0]+"\n");
            bw.close();
        }catch (Exception e){
            System.out.println("Data has not been written!");
        }
    }

    public void creatingSecondTShirt(String ... a){
        secondTShirt = new File("Second T-Shirt.txt");
        assigningReferances(secondTShirt);
        try {
            //name
            bw.write("New Generation T-Shirt \n");
            //color
            bw.write("Black\n");
            //--
            bw.write(" - \n");
            //price
            bw.write("29.90 AZN \n");
            //adding
            bw.write(a[0]+"\n");
            bw.close();
        }catch (Exception e){
            System.out.println("Data has not been written!");
        }
    }

    public void creatingWatch(){
        watch = new File("Watch.txt");
        assigningReferances(watch);
        try {
            //name
            bw.write("Leather Watch \n");
            //color
            bw.write("Brown\n");
            //--
            bw.write(" - \n");
            //price
            bw.write("49.90 AZN \n");
            bw.close();
        }catch (Exception e){
            System.out.println("Data has not been written!");
        }
    }

    public void creatingSunglasses(){
        sunglasses = new File("Sunglasses.txt");
        assigningReferances(sunglasses);
        try {
            //name
            bw.write("Sunglasses \n");
            //color
            bw.write("Orange\n");
            //--
            bw.write(" - \n");
            //price
            bw.write("39.90 AZN \n");
            bw.close();
        }catch (Exception e){
            System.out.println("Data has not been written!");
        }
    }

}
