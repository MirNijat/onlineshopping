package Frames;

import Database.CreatingFiles;
import Database.PurchasedItems;
import Database.Shoes;

import javax.swing.*;

public class JFrameShoes extends JFrame{
    private JPanel ShoesPage;
    private JButton homeButton;
    private JLabel shoesHeading;
    private JLabel blackNike;
    private JLabel olcu1;
    private JRadioButton size39RadioButton1;
    private JRadioButton size40RadioButton1;
    private JRadioButton size41RadioButton1;
    private JRadioButton size42RadioButton1;
    private JRadioButton size43RadioButton1;
    private JButton cartButton;
    private JLabel priceTag1;
    private JButton addToCart1;
    private JLabel sk8Hi;
    private JLabel olcu2;
    private JRadioButton size39RadioButton2;
    private JRadioButton size40RadioButton2;
    private JRadioButton size41RadioButton2;
    private JRadioButton size42RadioButton2;
    private JRadioButton size43RadioButton2;
    private JLabel priceTag2;
    private JButton addToCart2;



    JFrameShoes() {
        Shoes.assigningReferances();
        add(ShoesPage);
        setTitle("Shoes category");
        setSize(1200, 820);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        addToCart1.addActionListener(e -> {
            CreatingFiles.cf = new CreatingFiles();
            if (size39RadioButton1.isSelected()){
                CreatingFiles.cf.creatingNikeAirMax("Size: 39");
                PurchasedItems.orders.put(Shoes.NikeAirMax, CreatingFiles.NikeAirMax);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (size40RadioButton1.isSelected()){
                CreatingFiles.cf.creatingNikeAirMax("Size: 40");
                PurchasedItems.orders.put(Shoes.NikeAirMax, CreatingFiles.NikeAirMax);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (size41RadioButton1.isSelected()){
                CreatingFiles.cf.creatingNikeAirMax("Size: 41");
                PurchasedItems.orders.put(Shoes.NikeAirMax, CreatingFiles.NikeAirMax);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (size42RadioButton1.isSelected()){
                CreatingFiles.cf.creatingNikeAirMax("Size: 42");
                PurchasedItems.orders.put(Shoes.NikeAirMax, CreatingFiles.NikeAirMax);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (size43RadioButton1.isSelected()){
                CreatingFiles.cf.creatingNikeAirMax("Size: 43");
                PurchasedItems.orders.put(Shoes.NikeAirMax, CreatingFiles.NikeAirMax);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(null, "Choose Size", "Size is not chosen", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        addToCart2.addActionListener(e -> {
            CreatingFiles.cf = new CreatingFiles();
            if (size39RadioButton2.isSelected()){
                CreatingFiles.cf.creatingSK8_HI("Size: 39");
                PurchasedItems.orders.put(Shoes.SK8_HI, CreatingFiles.SK8_HI);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (size40RadioButton2.isSelected()){
                CreatingFiles.cf.creatingSK8_HI("Size: 40");
                PurchasedItems.orders.put(Shoes.SK8_HI, CreatingFiles.SK8_HI);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (size41RadioButton2.isSelected()){
                CreatingFiles.cf.creatingSK8_HI("Size: 41");
                PurchasedItems.orders.put(Shoes.SK8_HI, CreatingFiles.SK8_HI);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (size42RadioButton2.isSelected()){
                CreatingFiles.cf.creatingSK8_HI("Size: 42");
                PurchasedItems.orders.put(Shoes.SK8_HI, CreatingFiles.SK8_HI);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (size43RadioButton2.isSelected()){
                CreatingFiles.cf.creatingSK8_HI("Size: 43");
                PurchasedItems.orders.put(Shoes.SK8_HI, CreatingFiles.SK8_HI);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(null, "Choose Size", "Size is not chosen", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        size43RadioButton1.addActionListener(e -> {
            if(size43RadioButton1.isSelected()) {
                size39RadioButton1.setSelected(false);
                size40RadioButton1.setSelected(false);
                size41RadioButton1.setSelected(false);
                size42RadioButton1.setSelected(false);
            }
        });
        size39RadioButton2.addActionListener(e -> {
            if(size39RadioButton2.isSelected()) {
                size40RadioButton2.setSelected(false);
                size41RadioButton2.setSelected(false);
                size42RadioButton2.setSelected(false);
                size43RadioButton2.setSelected(false);
            }
        });
        size40RadioButton2.addActionListener(e -> {
            if(size40RadioButton2.isSelected()) {
                size39RadioButton2.setSelected(false);
                size41RadioButton2.setSelected(false);
                size42RadioButton2.setSelected(false);
                size43RadioButton2.setSelected(false);
            }
        });
        size41RadioButton2.addActionListener(e -> {
            if(size41RadioButton2.isSelected()) {
                size39RadioButton2.setSelected(false);
                size40RadioButton2.setSelected(false);
                size42RadioButton2.setSelected(false);
                size43RadioButton1.setSelected(false);
            }
        });
        size42RadioButton2.addActionListener(e -> {
            if(size42RadioButton2.isSelected()) {
                size39RadioButton2.setSelected(false);
                size40RadioButton2.setSelected(false);
                size41RadioButton2.setSelected(false);
                size43RadioButton2.setSelected(false);
            }
        });
        size43RadioButton2.addActionListener(e -> {
            if(size43RadioButton2.isSelected()) {
                size39RadioButton2.setSelected(false);
                size40RadioButton2.setSelected(false);
                size41RadioButton2.setSelected(false);
                size42RadioButton2.setSelected(false);
            }
        });

        homeButton.addActionListener(e -> {
            setVisible(false);
            new JFrameHomePage();
        });
        cartButton.addActionListener(e -> {
            setVisible(false);
            new JFrameCartPage();
        });
    }
}
