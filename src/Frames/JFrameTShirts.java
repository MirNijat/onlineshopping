package Frames;

import Database.CreatingFiles;
import Database.PurchasedItems;
import Database.TShirts;

import javax.swing.*;

public class JFrameTShirts extends JFrame{
    private JPanel TShirtPage;
    private JLabel programmerTShirt;
    private JButton addToCart1;
    private JButton addToCart2;
    private JLabel secondTShirt;
    private JLabel tShirtHeading;
    private JLabel priceTag1;
    private JLabel priceTag2;
    private JRadioButton sRadioButton1;
    private JLabel olcu1;
    private JRadioButton mRadioButton1;
    private JRadioButton lRadioButton1;
    private JRadioButton xsRadioButton1;
    private JRadioButton xlRadioButton1;
    private JLabel olcu2;
    private JRadioButton xsRadioButton2;
    private JRadioButton sRadioButton2;
    private JRadioButton mRadioButton2;
    private JRadioButton lRadioButton2;
    private JRadioButton xlRadioButton2;
    private JButton cartButton;
    private JButton homeButton;


    JFrameTShirts() {
        TShirts.assigningReferances();
        add(TShirtPage);
        setTitle("T-Shirt category");
        setSize(1080, 800);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        addToCart1.addActionListener(e -> {
            CreatingFiles.cf = new CreatingFiles();
            if (xsRadioButton1.isSelected()){
                CreatingFiles.cf.creatingProgrammerTShirt("Size: XS");
                PurchasedItems.orders.put(TShirts.programmerTShirt, CreatingFiles.programmerTShirt);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (sRadioButton1.isSelected()){
                CreatingFiles.cf.creatingProgrammerTShirt("Size: S");
                PurchasedItems.orders.put(TShirts.programmerTShirt, CreatingFiles.programmerTShirt);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (mRadioButton1.isSelected()){
                CreatingFiles.cf.creatingProgrammerTShirt("Size: M");
                PurchasedItems.orders.put(TShirts.programmerTShirt, CreatingFiles.programmerTShirt);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (lRadioButton1.isSelected()){
                CreatingFiles.cf.creatingProgrammerTShirt("Size: L");
                PurchasedItems.orders.put(TShirts.programmerTShirt, CreatingFiles.programmerTShirt);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (xlRadioButton1.isSelected()){
                CreatingFiles.cf.creatingProgrammerTShirt("Size: XL");
                PurchasedItems.orders.put(TShirts.programmerTShirt, CreatingFiles.programmerTShirt);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(null, "Choose Size", "Size is not chosen", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        addToCart2.addActionListener(e -> {
            CreatingFiles.cf = new CreatingFiles();
            if (xsRadioButton2.isSelected()){
                CreatingFiles.cf.creatingSecondTShirt("Size: XS");
                PurchasedItems.orders.put(TShirts.secondTShirt, CreatingFiles.secondTShirt);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (sRadioButton2.isSelected()){
                CreatingFiles.cf.creatingSecondTShirt("Size: S");
                PurchasedItems.orders.put(TShirts.secondTShirt, CreatingFiles.secondTShirt);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (mRadioButton2.isSelected()){
                CreatingFiles.cf.creatingSecondTShirt("Size: M");
                PurchasedItems.orders.put(TShirts.secondTShirt, CreatingFiles.secondTShirt);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (lRadioButton2.isSelected()){
                CreatingFiles.cf.creatingSecondTShirt("Size: L");
                PurchasedItems.orders.put(TShirts.secondTShirt, CreatingFiles.secondTShirt);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (xlRadioButton2.isSelected()){
                CreatingFiles.cf.creatingSecondTShirt("Size: XL");
                PurchasedItems.orders.put(TShirts.secondTShirt, CreatingFiles.secondTShirt);
                JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(null, "Choose Size", "Size is not chosen", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        //radio buttons
        xsRadioButton1.addActionListener(e -> {
            if(xsRadioButton1.isSelected()){
                sRadioButton1.setSelected(false);
                mRadioButton1.setSelected(false);
                lRadioButton1.setSelected(false);
                xlRadioButton1.setSelected(false);
            }
        });
        sRadioButton1.addActionListener(e -> {
            if(sRadioButton1.isSelected()) {
                xsRadioButton1.setSelected(false);
                mRadioButton1.setSelected(false);
                lRadioButton1.setSelected(false);
                xlRadioButton1.setSelected(false);
            }
        });
        mRadioButton1.addActionListener(e -> {
            if(mRadioButton1.isSelected()) {
                xsRadioButton1.setSelected(false);
                sRadioButton1.setSelected(false);
                lRadioButton1.setSelected(false);
                xlRadioButton1.setSelected(false);
            }
        });
        lRadioButton1.addActionListener(e -> {
            if(lRadioButton1.isSelected()) {
                xsRadioButton1.setSelected(false);
                sRadioButton1.setSelected(false);
                mRadioButton1.setSelected(false);
                xlRadioButton1.setSelected(false);
            }
        });
        xlRadioButton1.addActionListener(e -> {
            if(xlRadioButton1.isSelected()) {
                xsRadioButton1.setSelected(false);
                sRadioButton1.setSelected(false);
                mRadioButton1.setSelected(false);
                lRadioButton1.setSelected(false);
            }
        });
        xsRadioButton2.addActionListener(e -> {
            if(xsRadioButton2.isSelected()) {
                sRadioButton2.setSelected(false);
                mRadioButton2.setSelected(false);
                lRadioButton2.setSelected(false);
                xlRadioButton2.setSelected(false);
            }
        });
        sRadioButton2.addActionListener(e -> {
            if(sRadioButton2.isSelected()) {
                xsRadioButton2.setSelected(false);
                mRadioButton2.setSelected(false);
                lRadioButton2.setSelected(false);
                xlRadioButton2.setSelected(false);
            }
        });
        mRadioButton2.addActionListener(e -> {
            if(mRadioButton2.isSelected()) {
                xsRadioButton2.setSelected(false);
                sRadioButton2.setSelected(false);
                lRadioButton2.setSelected(false);
                xlRadioButton1.setSelected(false);
            }
        });
        lRadioButton2.addActionListener(e -> {
            if(lRadioButton2.isSelected()) {
                xsRadioButton2.setSelected(false);
                sRadioButton2.setSelected(false);
                mRadioButton2.setSelected(false);
                xlRadioButton2.setSelected(false);
            }
        });
        xlRadioButton2.addActionListener(e -> {
            if(xlRadioButton2.isSelected()) {
                xsRadioButton2.setSelected(false);
                sRadioButton2.setSelected(false);
                mRadioButton2.setSelected(false);
                lRadioButton2.setSelected(false);
            }
        });

        homeButton.addActionListener(e -> {
            setVisible(false);
            new JFrameHomePage();
        });
        cartButton.addActionListener(e -> {
            setVisible(false);
            new JFrameCartPage();
        });
    }
}
