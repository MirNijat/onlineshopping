package Frames;

import javax.swing.*;

public class JFrameHomePage extends javax.swing.JFrame {
    private JPanel rootPanel;
    private JPanel homePage;
    private JLabel logo;
    private JLabel welcome;
    private JButton tShirtButton;
    private JButton shoesButton;
    private JButton accessoriesButton;
    private JButton cartButton;

    JFrameHomePage() {
        add(rootPanel);
        setTitle("1Click Shopping");
        setSize(1080, 720);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        tShirtButton.addActionListener(e -> {
            setVisible(false);
            new JFrameTShirts();
        });
        accessoriesButton.addActionListener(e -> {
            setVisible(false);
            new JFrameAccessories();
        });
        shoesButton.addActionListener(e -> {
            setVisible(false);
            new JFrameShoes();
        });
        cartButton.addActionListener(e -> {
            setVisible(false);
            new JFrameCartPage();
        });
    }
}
