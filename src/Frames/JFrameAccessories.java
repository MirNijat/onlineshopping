package Frames;

import Database.Accessories;
import Database.CreatingFiles;
import Database.PurchasedItems;

import javax.swing.*;

public class JFrameAccessories extends JFrame{
    private JPanel AccessoriesPage;
    private JLabel heading;
    private JLabel watch;
    private JLabel sunglasses;
    private JLabel priceTag1;
    private JLabel priceTag2;
    private JButton addToCart1;
    private JButton addToCart2;
    private JButton homeButton;
    private JButton cartButton;

    JFrameAccessories(){
        Accessories.assigningReferances();
        add(AccessoriesPage);
        setTitle("Accessories category");
        setSize(1125, 825);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        addToCart1.addActionListener(e -> {
            CreatingFiles.cf = new CreatingFiles();
            CreatingFiles.cf.creatingWatch();
            PurchasedItems.orders.put(Accessories.watch, CreatingFiles.watch);
            JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
        });

        addToCart2.addActionListener(e -> {
            CreatingFiles.cf = new CreatingFiles();
            CreatingFiles.cf.creatingSunglasses();
            PurchasedItems.orders.put(Accessories.sunglasses, CreatingFiles.sunglasses);
            JOptionPane.showMessageDialog(null, "Added to cart", "Successful", JOptionPane.INFORMATION_MESSAGE);
        });
        homeButton.addActionListener(e -> {
            setVisible(false);
            new JFrameHomePage();
        });
        cartButton.addActionListener(e -> {
            setVisible(false);
            new JFrameCartPage();
        });
    }
}
