package Frames;

import Database.PurchasedItems;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class JFrameCartPage extends JFrame{
    private JPanel cartPage;
    private JLabel heading;
    private JButton homeButton;
    private JLabel products;
    private JLabel totalPrice;
    private JTextField totalPriceTF;
    private JLabel odemeUsulu;
    private JRadioButton cashRadioButton;
    private JRadioButton byCardRadioButton;
    private JButton payButton;
    private JTextArea namesOfProducts;

    JFrameCartPage(){
        add(cartPage);
        setTitle("Cart");
        setSize(1080, 720);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        homeButton.addActionListener(e -> {
            setVisible(false);
            new JFrameHomePage();
        });
        cashRadioButton.addActionListener(e -> {
            if(cashRadioButton.isSelected()){
                byCardRadioButton.setSelected(false);
            }
        });
        byCardRadioButton.addActionListener(e -> {
            if (byCardRadioButton.isSelected()){
                cashRadioButton.setSelected(false);
            }
        });
        payButton.addActionListener(e -> {
            if (byCardRadioButton.isSelected()){
                JOptionPane.showMessageDialog(null, "Card payment is unavailable", "Error", JOptionPane.INFORMATION_MESSAGE);
            }
            else if (cashRadioButton.isSelected()){
                JOptionPane.showMessageDialog(null, "Ordered successfully", "Successful", JOptionPane.INFORMATION_MESSAGE);
                setDefaultCloseOperation(EXIT_ON_CLOSE);
                setVisible(false);
                System.exit(0);
            }
            else{
                JOptionPane.showMessageDialog(null, "Payment method is not selected", "Error", JOptionPane.INFORMATION_MESSAGE);
            }
        });
    }

    public void setVisible(boolean bool){
        int a = 1;
        double totalPriceD = 0;
        NumberFormat nf = new DecimalFormat("#0.00");
        for (File file : PurchasedItems.orders.values()){
            try {
                //writing names of products
                namesOfProducts.setText(namesOfProducts.getText() + a + ". ");
                a++;
                BufferedReader buff = new BufferedReader(new FileReader(file));
                for(; ; ) {
                    if(buff.ready()) {
                            namesOfProducts.setText(namesOfProducts.getText() + buff.readLine());
                    }
                    else break;
                }

            }catch(Exception e){
                System.out.println(e.getMessage());
            }
            namesOfProducts.setText(namesOfProducts.getText() + "\n");


            try {
                //writing total price
                BufferedReader buff = new BufferedReader(new FileReader(file));
                String line;
                while (buff.ready()) {
                    line = buff.readLine();
                    if (line.equals(" - ")) {
                        line = buff.readLine();
                        totalPriceD += Double.parseDouble((line.substring(0, 4)));
                    }
                }
                totalPriceTF.setText(nf.format(totalPriceD) + " AZN");
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }


        super.setVisible(bool);
    }
}
